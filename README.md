# djrepec
`djrepec` is a "fast hacked app" to provide a python based [REPEC](https://ideas.repec.org/stepbystep.html) repository 
endpoint aimed to replace [repecphp](https://sourceforge.net/projects/repecphp/), a previous tool we were using. It's a 
very simple app, for a quite simple protocol designed for our organization needs, but it can be useful for yours too. 
It's based on django's admin interface, which is used to manage the repository.

`djrepec` only needs Django 1.8 and comes configured for SQLite, but you can use any other supported 
database because the model is plain simple.

RDF files are created on the fly and there's no cache implemented. If you manage a large repository it would be
an interesting idea.

## Model
Following the RePeC model, the main entities in this repository are:

- __Archive__: contains the paper series. Multiple archives can be managed.

- __Series__: a collection of papers.

- __Paper__: it's the only resource type allowed/used in our organizatin at the time being.

- __File__: any file resources related to a paper.

- __Person__: the authors.

- __Organization__: the author's organization.

- __Institution__: an organization belongs to.

## Disclaimer
This app was coded in a day (more than six months ago) and was designed, if we could call it design, on the fly to meet 
the basic RePeC specifications. Any improvement and extension will be welcomed ;)

The html interface was only designed for testing.

## Setup
Configure the webapp in your server and edit the settings file as needed to prepare it for production:

- ALLOWED_HOSTS
- DATABASES
- SECRET_KEY

Run `python manage migrate` to create the database and an admin user.

Run the server and access `/admin` to create an archive and paper series.

The RDF files are created when requested at the corresponding endpoints.

;)