from datetime import datetime
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models

REPEC_HANDLE_PREFIX = 'RePEc'
REPEC_HANDLE_SEP = ':'

SERIES_TYPE_CHOICES = (
    ("ReDIF-Paper", "Paper"),
    ("ReDIF-Software", "Software"),
    ("ReDIF-Article", "Article"),
    ("ReDIF-Chapter", "Chapter"),
    ("ReDIF-Book", "Book")
)

PAPER_PUBLICATION_TYPE_CHOICES = (
    ("journal article", "journal article"),
    ("book", "book"),
    ("book chapter", "book chapter"),
    ("working paper", "working paper"),
    ("conference paper", "conference paper"),
    ("report", "report"),
    ("other", "other")
)

FILE_TYPE_CHOICES = (
    ("application/pdf", "pdf"),
    ("application/postscript", "ps"),
    ("application/application/vnd.openxmlformats-officedocument.wordprocessingml.document", "docx"),
    ("application/msword", "doc")
)


class TraceableModel(models.Model):
    _created_on = models.DateTimeField(auto_now_add=True)
    _created_by = models.ForeignKey(User, related_name='+')
    _last_modified_on = models.DateTimeField(auto_now=True, null=True, blank=True)
    _last_modified_by = models.ForeignKey(User, related_name='+', null=True, blank=True)

    class Meta:
        abstract = True

    def last_modified(self):
        if self._last_modified_on:
            return self._last_modified_on
        else:
            return self._created_on or datetime.now()

    """
    def recursive_last_modified(self):
        for f in self._meta.get_fields():
            if f.is_relation and isinstance(f.related_model(), TraceableModel):
                print(f.related_model)
    """


class ReDIFResource(TraceableModel):
    _resource_type = "undefined"

    class Meta:
        abstract = True


class Archive(ReDIFResource):
    _resource_type = "archive"

    handle = models.CharField(max_length=255, unique=True,
                              validators=[RegexValidator(regex='^[a-z]{3}$',
                                                         message='handle must be a THREE letter string')])
    name = models.CharField(max_length=255)
    url = models.URLField(max_length=255)
    maintainer_name = models.CharField(max_length=255)
    maintainer_email = models.EmailField(max_length=255)
    maintainer_phone = models.CharField(max_length=255, blank=True)
    maintainer_fax = models.CharField(max_length=255, blank=True)
    homepage = models.URLField(max_length=255)
    description = models.TextField()

    def handle_full(self):
        return REPEC_HANDLE_SEP.join([REPEC_HANDLE_PREFIX, self.handle])

    def __unicode__(self):
        return self.name


class Series(ReDIFResource):
    _resource_type = 'series'

    handle = models.CharField(max_length=255,
                              validators=[RegexValidator(regex='^[a-z]{6}$',
                                                         message='handle must be a SIX letter string')])
    archive = models.ForeignKey('Archive', related_name='series')
    name = models.CharField(max_length=255)
    maintainer_name = models.CharField(max_length=255, blank=True)
    maintainer_email = models.EmailField(max_length=255)
    maintainer_phone = models.CharField(max_length=255, blank=True)
    maintainer_fax = models.CharField(max_length=255, blank=True)
    editor = models.ForeignKey('Person')
    provider = models.ForeignKey('Organization')
    type = models.CharField(max_length=32, choices=SERIES_TYPE_CHOICES, default=SERIES_TYPE_CHOICES[0][0])
    description = models.TextField()

    class Meta:
        unique_together = ('handle', 'archive')
        verbose_name_plural = "series"

    def handle_full(self):
        return REPEC_HANDLE_SEP.join([self.archive.handle_full(), self.handle])

    def __unicode__(self):
        return self.name


class Paper(ReDIFResource):
    _resource_type = 'paper'

    series = models.ForeignKey('Series', related_name='papers')
    title = models.CharField(max_length=255)
    abstract = models.TextField()
    note = models.TextField(null=True, blank=True)
    authors = models.ManyToManyField('Person')
    number = models.CharField(max_length=64)
    length = models.CharField(max_length=64, null=True, blank=True)
    keywords = models.CharField(max_length=255)
    classification_jel = models.CharField(max_length=255)
    creation_date = models.DateField(null=True, blank=True)
    revision_date = models.DateField(null=True, blank=True)
    publication_type = models.CharField(max_length=32, choices=PAPER_PUBLICATION_TYPE_CHOICES, null=True, blank=True)

    rdf_fields = (
        'Title',
        'Abstract',
        'Number',
        'Creation-Date',
        'Revision-Date',
        'Publication-Type'
    )

    @property
    def handle(self):
        return self.number

    @handle.setter
    def handle(self, value):
        self.number = value

    class Meta:
        unique_together = ('series', 'number')

    def handle_full(self):
        return REPEC_HANDLE_SEP.join([self.series.handle_full(), self.number])

    def __unicode__(self):
        return self.title

    def last_modified(self):
        latest_modified_on = super(Paper, self).last_modified()

        for author in self.authors.iterator():
            latest_modified_on = max(latest_modified_on, author.last_modified())

        for f in self.files.iterator():
            latest_modified_on = max(latest_modified_on, f.last_modified())

        return latest_modified_on


class Organization(TraceableModel):
    name = models.CharField(unique=True, max_length=255)
    homepage = models.URLField(max_length=255)
    name_english = models.CharField(max_length=255, blank=True)
    postal = models.CharField(max_length=255, blank=True)
    location = models.CharField(max_length=255, blank=True)
    email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=255, blank=True)
    fax = models.CharField(max_length=255, blank=True)
    institution = models.ForeignKey('Institution', null=True, blank=True)

    def __unicode__(self):
        return self.name


class Institution(TraceableModel):
    handle = models.CharField(max_length=255, unique=True)
    primary = models.ForeignKey('Organization', related_name='primaries', null=True, blank=True)
    secondary = models.ForeignKey('Organization', related_name='secodaries', blank=True, null=True)

    #     tertiary = models.ForeignKey('Organization', related_name='tertiaries', blank=True)
    #     quaternary = models.ForeignKey('Organization', related_name='quaternaries', blank=True)

    def __unicode__(self):
        return self.handle

    def handle_full(self):
        return self.handle


class Person(TraceableModel):
    name_first = models.CharField(max_length=255)
    name_last = models.CharField(max_length=255)
    name_prefix = models.CharField(max_length=32, blank=True)
    name_middle = models.CharField(max_length=32, blank=True)
    name_suffix = models.CharField(max_length=32, blank=True)
    # name_ascii
    # name_full
    email = models.EmailField(null=True, blank=True)
    workplace = models.ForeignKey('Organization', null=True, blank=True)
    short_id = models.CharField(max_length=16, null=True, blank=True)

    class Meta:
        ordering = ('name_first', 'name_last',)

    @property
    def name(self):
        full_name = u"{first} {middle} {last}".format(
            first=self.name_first,
            middle=self.name_middle,
            last=self.name_last)
        # version funcional. mejorar con RE
        return full_name.replace('  ', ' ').strip()

    def __unicode__(self):
        return self.name

    def last_modified(self):
        obj_last_modified_on = super(Person, self).last_modified()
        if self.workplace:
            return max(self.workplace.last_modified(), obj_last_modified_on)
        else:
            return obj_last_modified_on


class File(TraceableModel):
    paper = models.ForeignKey('Paper', related_name='files')
    url = models.URLField(max_length=255)
    format = models.CharField(max_length=128, choices=FILE_TYPE_CHOICES, default='application/pdf')
    function = models.CharField(max_length=255, null=True, blank=True)
    restriction = models.CharField(max_length=255, null=True, blank=True)
