from django.utils.safestring import mark_safe

__author__ = 'asier'

from django import template

register = template.Library()

def rdf_field2attr(fieldname):
    return fieldname.replace('-', '_').lower()

def rdf_formatvalue(value):
    if isinstance(value, unicode):
        return value.replace('\n', '\n ')
    else:
        return unicode(value)

@register.filter(name='rdf_field')
def rdf_field(value, fieldname):
    if value:
        return mark_safe(u'{0}: {1}'.format(fieldname, rdf_formatvalue(value)))
    return ''