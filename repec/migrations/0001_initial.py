# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Archive',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_created_on', models.DateTimeField(auto_now_add=True)),
                ('_last_modified_on', models.DateTimeField(auto_now=True, null=True)),
                ('handle', models.CharField(unique=True, max_length=255, validators=[django.core.validators.RegexValidator(regex=b'^[a-z]{3}$', message=b'handle must be a THREE letter string')])),
                ('name', models.CharField(max_length=255)),
                ('url', models.URLField(max_length=255)),
                ('maintainer_name', models.CharField(max_length=255)),
                ('maintainer_email', models.EmailField(max_length=255)),
                ('maintainer_phone', models.CharField(max_length=255, blank=True)),
                ('maintainer_fax', models.CharField(max_length=255, blank=True)),
                ('homepage', models.URLField(max_length=255)),
                ('description', models.TextField()),
                ('_created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('_last_modified_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_created_on', models.DateTimeField(auto_now_add=True)),
                ('_last_modified_on', models.DateTimeField(auto_now=True, null=True)),
                ('url', models.URLField(max_length=255)),
                ('format', models.CharField(default=b'application/pdf', max_length=128, choices=[(b'application/pdf', b'pdf'), (b'application/postscript', b'ps'), (b'application/application/vnd.openxmlformats-officedocument.wordprocessingml.document', b'docx'), (b'application/msword', b'doc')])),
                ('function', models.CharField(max_length=255, null=True, blank=True)),
                ('restriction', models.CharField(max_length=255, null=True, blank=True)),
                ('_created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('_last_modified_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Institution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_created_on', models.DateTimeField(auto_now_add=True)),
                ('_last_modified_on', models.DateTimeField(auto_now=True, null=True)),
                ('handle', models.CharField(unique=True, max_length=255)),
                ('_created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('_last_modified_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_created_on', models.DateTimeField(auto_now_add=True)),
                ('_last_modified_on', models.DateTimeField(auto_now=True, null=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('homepage', models.URLField(max_length=255)),
                ('name_english', models.CharField(max_length=255, blank=True)),
                ('postal', models.CharField(max_length=255, blank=True)),
                ('location', models.CharField(max_length=255, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('phone', models.CharField(max_length=255, blank=True)),
                ('fax', models.CharField(max_length=255, blank=True)),
                ('_created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('_last_modified_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('institution', models.ForeignKey(blank=True, to='repec.Institution', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Paper',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_created_on', models.DateTimeField(auto_now_add=True)),
                ('_last_modified_on', models.DateTimeField(auto_now=True, null=True)),
                ('title', models.CharField(max_length=255)),
                ('abstract', models.TextField()),
                ('note', models.TextField(null=True, blank=True)),
                ('number', models.CharField(max_length=64)),
                ('length', models.CharField(max_length=64, null=True, blank=True)),
                ('keywords', models.CharField(max_length=255)),
                ('classification_jel', models.CharField(max_length=255)),
                ('creation_date', models.DateField(null=True, blank=True)),
                ('revision_date', models.DateField(null=True, blank=True)),
                ('publication_type', models.CharField(blank=True, max_length=32, null=True, choices=[(b'journal article', b'journal article'), (b'book', b'book'), (b'book chapter', b'book chapter'), (b'working paper', b'working paper'), (b'conference paper', b'conference paper'), (b'report', b'report'), (b'other', b'other')])),
                ('_created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('_last_modified_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_created_on', models.DateTimeField(auto_now_add=True)),
                ('_last_modified_on', models.DateTimeField(auto_now=True, null=True)),
                ('name_first', models.CharField(max_length=255)),
                ('name_last', models.CharField(max_length=255)),
                ('name_prefix', models.CharField(max_length=32, blank=True)),
                ('name_middle', models.CharField(max_length=32, blank=True)),
                ('name_suffix', models.CharField(max_length=32, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('short_id', models.CharField(max_length=16, null=True, blank=True)),
                ('_created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('_last_modified_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('workplace', models.ForeignKey(blank=True, to='repec.Organization', null=True)),
            ],
            options={
                'ordering': ('name_first', 'name_last'),
            },
        ),
        migrations.CreateModel(
            name='Series',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('_created_on', models.DateTimeField(auto_now_add=True)),
                ('_last_modified_on', models.DateTimeField(auto_now=True, null=True)),
                ('handle', models.CharField(max_length=255, validators=[django.core.validators.RegexValidator(regex=b'^[a-z]{6}$', message=b'handle must be a SIX letter string')])),
                ('name', models.CharField(max_length=255)),
                ('maintainer_name', models.CharField(max_length=255, blank=True)),
                ('maintainer_email', models.EmailField(max_length=255)),
                ('maintainer_phone', models.CharField(max_length=255, blank=True)),
                ('maintainer_fax', models.CharField(max_length=255, blank=True)),
                ('type', models.CharField(default=b'ReDIF-Paper', max_length=32, choices=[(b'ReDIF-Paper', b'Paper'), (b'ReDIF-Software', b'Software'), (b'ReDIF-Article', b'Article'), (b'ReDIF-Chapter', b'Chapter'), (b'ReDIF-Book', b'Book')])),
                ('description', models.TextField()),
                ('_created_by', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('_last_modified_by', models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True)),
                ('archive', models.ForeignKey(related_name='series', to='repec.Archive')),
                ('editor', models.ForeignKey(to='repec.Person')),
                ('provider', models.ForeignKey(to='repec.Organization')),
            ],
            options={
                'verbose_name_plural': 'series',
            },
        ),
        migrations.AddField(
            model_name='paper',
            name='authors',
            field=models.ManyToManyField(to='repec.Person'),
        ),
        migrations.AddField(
            model_name='paper',
            name='series',
            field=models.ForeignKey(related_name='papers', to='repec.Series'),
        ),
        migrations.AddField(
            model_name='institution',
            name='primary',
            field=models.ForeignKey(related_name='primaries', blank=True, to='repec.Organization', null=True),
        ),
        migrations.AddField(
            model_name='institution',
            name='secondary',
            field=models.ForeignKey(related_name='secodaries', blank=True, to='repec.Organization', null=True),
        ),
        migrations.AddField(
            model_name='file',
            name='paper',
            field=models.ForeignKey(related_name='files', to='repec.Paper'),
        ),
        migrations.AlterUniqueTogether(
            name='series',
            unique_together=set([('handle', 'archive')]),
        ),
        migrations.AlterUniqueTogether(
            name='paper',
            unique_together=set([('series', 'number')]),
        ),
    ]
