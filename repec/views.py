from datetime import datetime
from django.shortcuts import render, get_object_or_404
from django.utils.http import http_date
from django.utils.timezone import utc
from .models import Archive, Series, Paper
from .rdf import rdf_printer


RDF_CONTENT_TYPE = "text/rdf; charset=utf-8"

_EPOCH = datetime(1970,1,1, tzinfo=utc)


def epoch(d):
    return (d - _EPOCH).total_seconds()


def redif_response(request, template, redif_resource):
    context = {}
    context[redif_resource._resource_type] = redif_resource
    response = render(request, template, context, content_type=RDF_CONTENT_TYPE)
    response['Last-Modified'] = http_date(epoch(redif_resource.last_modified()))
    return response


def archive_index(request, handle):
    archive = get_object_or_404(Archive, handle=handle)
    return render(request, 'archive.index.html', locals())


def archive_rdf(request, handle):
    archive = get_object_or_404(Archive, handle=handle)
    return redif_response(request, 'archive.rdf', archive)


def series_rdf(request, handle):
    archive = get_object_or_404(Archive, handle=handle)
    return redif_response(request, 'archive.series.rdf', archive)


def series_index(request, archive_handle, series_handle):
    series = get_object_or_404(Series, archive__handle=archive_handle, handle=series_handle)
    return render(request, 'series.index.html', locals())


def series_content_rdf(request, archive_handle, series_handle):
    series = get_object_or_404(Series,
                               handle=series_handle,
                               archive__handle=archive_handle)
    return render(request, 'series.content.rdf', series)


# @last_modified(paper_last_modified)
def paper_rdf(request, archive_handle, series_handle, paper_number):
    paper = get_object_or_404(Paper,
                              number=paper_number,
                              series__handle=series_handle,
                              series__archive__handle=archive_handle)
    response = redif_response(request, 'paper.rdf', paper)
    return response


def archive_list(request):
    objects = Archive.objects.all()
    return render(request, 'archive.list.html', locals())

