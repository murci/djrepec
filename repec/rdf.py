__author__ = 'asier'


def rdf_field2attr(fieldname):
    return fieldname.replace('-', '_').lower()


def rdf_printer(object, fieldlist):
    output = ''
    for field in fieldlist:
        attrname = rdf_field2attr(field)
        val = getattr(object, attrname)
        if val:
            output += '{0}: {1}\n'.format(field, val)
    return output