__author__ = 'asier'

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.archive_list, name='archive.list'),
    url(r'^(?P<handle>[a-z]{3})/$', views.archive_index, name='archive.index'),
    url(r'^(?P<handle>[a-z]{3})/([a-z]{3})arch.rdf$', views.archive_rdf, name='archive.rdf'),
    url(r'^(?P<handle>[a-z]{3})/([a-z]{3})seri.rdf$', views.series_rdf, name='series.rdf'),
    url(r'^(?P<archive_handle>[a-z]{3})/(?P<series_handle>[a-z]{6})/$', views.series_index,
        name='series.index'),
    url(r'^(?P<archive_handle>[a-z]{3})/(?P<series_handle>[a-z]{6})/(?P<paper_number>\w*).rdf$', views.paper_rdf,
        name='paper.rdf'),
]
