import codecs
import re
from repec.views import RDF_CONTENT_TYPE

__author__ = 'asier'


def is_empty(s):
    return re.match(r'^\s*$', s)


def is_notempty(s):
    return not is_empty(s)


class BlankLineCleanerMiddleware(object):
    def process_response(self, request, response):
        if response['Content-Type'] == RDF_CONTENT_TYPE:
            # response.content = re.sub(r'\n+', '\n', response.content)
            response.content = codecs.BOM_UTF8 + '\n'.join(filter(is_notempty, response.content.split('\n')))
        return response
