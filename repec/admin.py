from django.contrib import admin
from django.db import models
from django.forms import Textarea
from .models import Archive, Person, Paper, Series, Organization, Institution, File

# basic customization
admin.site.site_header = "RePEc archive manager"
admin.site.index_title = "RePEc archive administration"

class TraceableModelAdmin(admin.ModelAdmin):
    exclude = ('_created_by', '_created_on', '_last_modified_by', '_last_modified_on')

    def save_model(self, request, obj, form, change):
        if change:
            obj._last_modified_by = request.user
        else:
            obj._created_by = request.user
        obj.save()


class ArchiveAdmin(TraceableModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('handle', 'name', 'url', 'description', 'homepage')
        }),
        ('Maintainer', {
            'fields': ('maintainer_name', 'maintainer_email', 'maintainer_phone', 'maintainer_fax')
        }),
    )



class SeriesAdmin(TraceableModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('archive', 'handle', 'name', 'type', 'description', 'editor', 'provider')
        }),
        ('Maintainer', {
            'fields': ('maintainer_name', 'maintainer_email', 'maintainer_phone', 'maintainer_fax')
        }),
    )


class FileInline(admin.TabularInline):
    exclude = ('_created_by', '_created_on', '_last_modified_by', '_last_modified_on')
    model = File
    extra = 0


class PaperAdmin(TraceableModelAdmin):
    list_display = ('number', '__str__', 'creation_date')
    list_filter = (
        'series__handle',
        ('creation_date', admin.DateFieldListFilter)
    )
    fieldsets = (
        ('Metadata', {
            'fields': ('series', 'number', 'creation_date', 'revision_date', 'publication_type')
        }),
        ('Basic info', {
            'fields': ('title', 'abstract', 'authors', 'keywords', 'classification_jel')
        }),
        ('Additional information', {
            'classes': 'collapse',
            'fields': ('length', 'note')
        })
    )
    inlines = [FileInline]
    filter_horizontal = ('authors',)

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for obj in formset.deleted_objects:
            obj.delete()
        for obj in formset.new_objects:
            obj._created_by = request.user
            obj.save()
        for instance in instances:
            instance._last_modified_by = request.user
            instance.save()
        formset.save_m2m()


class PersonAdmin(TraceableModelAdmin):
    list_display = ('name', 'workplace')


class OrganizationAdmin(TraceableModelAdmin):
    pass


class InstitutionAdmin(TraceableModelAdmin):
    pass


admin.site.register(Archive, ArchiveAdmin)
admin.site.register(Series, SeriesAdmin)
admin.site.register(Paper, PaperAdmin)
admin.site.register(Person, PersonAdmin)
admin.site.register(Organization, OrganizationAdmin)
admin.site.register(Institution, InstitutionAdmin)
