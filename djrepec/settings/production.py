from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = [ 'your hostnames' ]

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'create your own production secret key!'

# Static files (CSS, JavaScript, Images)
STATIC_URL = ''

STATIC_ROOT = ''

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'path to your database'
    }
}